FROM nginx:latest
COPY dist/mfe-admin/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf