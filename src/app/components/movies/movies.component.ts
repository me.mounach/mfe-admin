import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Movie } from '../../models/movie/movie';
import { MovieService } from '../../services/movie.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  movies: Array<Movie> = new Array();
  query!: string;
  selected: String | undefined;

  @Output() onMoviePicked: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private movieService: MovieService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  //-------------------
  pickMovie(movie: Movie) {
    this.selected = movie.id;
    this.onMoviePicked.emit(movie);
  }

  /* ----------------------- */
  searchMovies() {

    let promise = new Promise((resolve, reject) => {
      this.movieService.searchMovies(this.query)
     .subscribe((response: any) => {
       //console.log(response);
       this.movies = response.body.results;
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

}
