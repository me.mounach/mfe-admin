import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { AuthService } from 'src/app/services/auth.service';
import { App } from '../../models/app/app';
import { Cinema } from '../../models/cinema/cinema';
import { CinemaService } from '../../services/cinema.service';

@Component({
  selector: 'app-cinema',
  templateUrl: './cinema.component.html',
  styleUrls: ['./cinema.component.scss']
})
export class CinemaComponent implements OnInit {

  /* ----------------------- */
  cinema_id: any;
  cinema: Cinema = new Cinema();

  app: App = new App();

  constructor(
    private route: ActivatedRoute,
    private cinemaService: CinemaService,
    config: NgbModalConfig, 
    private router: Router,
    @Inject(SESSION_STORAGE) private storage: StorageService,
    private authService: AuthService
  ) { 
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {

    this.getUser().then((data: any) => {
      
      if(data.body.isLogged == true) {
        this.cinema_id = this.route.snapshot.paramMap.get("id");
        this.getCinemaInfos();
      }else{
        this.app.error = true; 
        this.app.email = undefined; 
        this.app.status = 'NOT AUTHORIZED';
        this.app.username = '*** NONE ***'; 
        this.app.role = undefined;
        
        this.storage.set("app", this.app);
        this.router.navigate(['/home']);
      }

    }).catch(err => {
        this.app.error = true; 
        this.app.email = undefined; 
        this.app.status = 'NOT AUTHORIZED';
        this.app.username = '*** NONE ***'; 
        this.app.role = undefined;
        
        this.storage.set("app", this.app);
        this.router.navigate(['/home']);
    });

  }

  //------------
  getUser() {
    let promise = new Promise((resolve, reject) => {

        this.authService.getUser()
        .subscribe((response: any) => {

          resolve(response);

        } ,
          (err: any) => {
          reject(err);
        }
      );
    });
    
    return promise;
  }


  /* ----------------------- */
  getCinemaInfos() {

    let promise = new Promise((resolve, reject) => {
      this.cinemaService.getOneCinema(this.cinema_id)
     .subscribe((response: any) => {
       //console.log(response);
       this.cinema = response.body.cinema;
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

}
