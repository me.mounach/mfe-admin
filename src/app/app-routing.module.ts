import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CinemaComponent } from './components/cinema/cinema.component';
import { CinemasComponent } from './components/cinemas/cinemas.component';

const routes: Routes = [
  { path: 'cinemas', component: CinemasComponent },
  { path: 'cinema/:id/settings', component: CinemaComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
